
package inversordepalabras;
import java.util.Scanner;

/**
 *
 * @author Antony Giomar
 */
public class InversorDePalabras {
    public static void main(String[] args) {
        
        String palabra = "", palabraInvertida = "";
        int longitudPalabra = 0;
        
        Scanner inc = new Scanner(System.in);
        
        System.out.println("Escribe una palabra o frase: ");
        palabra = inc.nextLine();
        
        longitudPalabra = palabra.length();
        
        while (longitudPalabra != 0) {
            
            palabraInvertida += palabra.substring(longitudPalabra -1, longitudPalabra);
            
            
            longitudPalabra--;
        }
        
        System.out.print("Palabra invertida: " + palabraInvertida);
        System.out.println("");
    }
    
}

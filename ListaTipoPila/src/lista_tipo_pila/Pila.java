package lista_tipo_pila;

import javax.swing.JOptionPane;

public class Pila {

    private Nodo UltimoValorIngresado;
    int tamano;
    String Lista = "";

    public Pila() {
        UltimoValorIngresado = null;
        tamano = 0;
    }

    /*Método para saber cuando la pila está vacia*/
    public boolean PilaVacia() {
        return UltimoValorIngresado == null;
    }

    /*Metodo para insertar nodo*/
    public void InsertarNodo(int nodo) {
        Nodo nuevo_nodo = new Nodo(nodo);
        nuevo_nodo.siguiente = UltimoValorIngresado;
        UltimoValorIngresado = nuevo_nodo;
        tamano++;

    }

    /*Método para eliminar nodo*/
    public int EliminarNodo() {
        int auxiliar = UltimoValorIngresado.informacion;
        tamano--;
        return auxiliar;
    }

    /*Metodo para conocer el ultimo valor ingresado*/
    public int MostrarUltimoValorIngresado() {
        return UltimoValorIngresado.informacion;
    }

    /*Metodo para conocer el tamaño de la pila*/
    public int TamanoPila() {
        return tamano;
    }

    /*Metodo para vaciar pila*/
    public void VaciarPila() {
        while (!PilaVacia()) {
            EliminarNodo();

        }
    }

    /*Metodo para mostrar el contenido de la pila*/
    public void MostrarValores() {
        Nodo recorrido = UltimoValorIngresado;

        while (recorrido != null) {
            Lista += recorrido.informacion + "\n";
            recorrido = recorrido.siguiente;

        }
        JOptionPane.showMessageDialog(null, Lista);
        Lista = "";
    }

}


package ejemplo;


public class FuncionesTrigonometricas {
    public static void main(String[] args) {
        
        double resultado=0;
        double anguloEnGrados=45;
        double anguloEnRadianes = Math.toRadians(anguloEnGrados);
        
        /*Seno*/
        resultado = Math.sin(anguloEnRadianes);
        System.out.println("Seno de "+anguloEnGrados + "° = "+resultado);
        /*coseno*/
        resultado = Math.cos(anguloEnRadianes);
        System.out.println("Coseno de "+anguloEnGrados + "° = "+resultado);
        /*Seno*/
        resultado = Math.tan(anguloEnRadianes);
        System.out.println("Tangente de "+anguloEnGrados + "° = "+resultado);
        
        
        double ValorIngresado = 0.707;
        /*Arco Coseno*/
        anguloEnRadianes = Math.acos(ValorIngresado);
        anguloEnGrados = Math.toDegrees(anguloEnRadianes);
        System.out.println("Arco coseno de "+ValorIngresado+" = "+anguloEnGrados+"°");
        
        /*Arco Seno*/
        anguloEnRadianes = Math.asin(ValorIngresado);
        anguloEnGrados = Math.toDegrees(anguloEnRadianes);
        System.out.println("Arco seno de "+ValorIngresado+" = "+anguloEnGrados+"°");
        
        /*Arco Coseno*/
        anguloEnRadianes = Math.atan(ValorIngresado);
        anguloEnGrados = Math.toDegrees(anguloEnRadianes);
        System.out.println("Arco tangente de "+ValorIngresado+" = "+anguloEnGrados+"°");
        
    }
    
}

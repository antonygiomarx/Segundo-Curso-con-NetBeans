   
    package clases;
    import java.util.Scanner;

    public abstract class ClasePadre_Abstracta {/*Se crea clase abstracta porque aquí irá el proceso*/

        protected int transacciones, retiro, deposito;/*Variables protegidas, se heredan*/
        private static int saldo;/*Única variable privada*/
        private String usuario;/*Variable usuario, esta se va a jalar desde clase principal*/

        Scanner entrada = new Scanner(System.in); 

            public void Operaciones(){/**/
               int bandera = 0;
               int seleccion = 0;
             do{/*Ciclo será infinito hasta que seleccionen opción 4*/

                 do{/*Ciclo será infinito*/
                   System.out.println("-----------------------------------------------------------");
                   System.out.println("Por favor seleccione una opción Sr. " + getUsuario() + ": ");
                   System.out.println("    1. Consulta de saldo.");
                   System.out.println("    2. Retiro de efectivo");
                   System.out.println("    3. Deposito de efectivo");
                   System.out.println("    4. Salir");
                   System.out.println("------------------------------------------------------------");
                   seleccion = entrada.nextInt();

                   if (seleccion >=1 && seleccion <=4) {
                       bandera = 1;

                   } else{
                       System.out.println("--------------------------------------------------------------------------");
                       System.out.println("Opción no disponible, vuelva a intentar con otra opción Sr." + getUsuario());
                       System.out.println("--------------------------------------------------------------------------");

                   }
               } while(bandera == 0);
                        if (seleccion == 1) {
                        ClasePadre_Abstracta mensajero = new ClaseHija_Consulta();
                        mensajero.Transacciones();


                     } else if (seleccion == 2) {
                         ClasePadre_Abstracta mensajero = new ClaseHija_Retiro();
                         mensajero.Transacciones();

                     }
                     else if (seleccion == 3) {
                         ClasePadre_Abstracta mensajero = new ClaseHija_Deposito();
                         mensajero.Transacciones();

                     }
                     else if (seleccion == 4) {
                         System.out.println("------------------------------------------");
                         System.out.println("Gracias, vuelva pronto Sr. " + getUsuario());
                         System.out.println("------------------------------------------");
                         bandera = 2;
                     }

                  } while(bandera != 2);/*Cierra el primer do*/
                 }

                // Método para solicitar cantidad de retiro

                public void Retiro(){
                    retiro = entrada.nextInt();

                }

                //Método para solicitar deposito
                public void Deposito(){
                    deposito = entrada.nextInt();

                }
                //Método abstracto
                public abstract void Transacciones();



                //Métodos setter & getter

                public int getSaldo(){
                    return saldo;

                }

                public void setSaldo(int saldo){
                    this.saldo = saldo;

                }

                public String getUsuario() {
                return usuario;
                } 

                public void setUsuario(String usuario){
                this.usuario = usuario;
                }
            }

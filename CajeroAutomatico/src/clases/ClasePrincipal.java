

    package clases;
    import java.util.Scanner;

    public class ClasePrincipal {
        public static String usuario;
        public static void main(String[] args) {


            Scanner entrada = new Scanner(System.in);


            ClasePadre_Abstracta mensajero = new ClaseHija_Consulta();
            System.out.println("¿Cuál es su nombre?");
            usuario = entrada.nextLine();
            mensajero.setUsuario(usuario);
            mensajero.setSaldo(50000);
            mensajero.Operaciones();



            /*
            ClasePadre_Abstracta mensajero = new ClaseHija_Consulta();
            mensajero.setSaldo(500);
            mensajero.Operaciones();
            */


        }
    }
package division;

import java.util.Scanner;

public class ClaseDivision {

    public static void main(String[] args) {

        try {
            Scanner input = new Scanner(System.in);

            int valor1, valor2, resultado;

            System.out.println("Dame el primer valor: ");
            valor1 = input.nextInt();

            System.out.println("Dame el segundo valor: ");
            valor2 = input.nextInt();

            resultado = valor1 / valor2;

            System.out.println("El resultado es: " + resultado);

        } catch (Exception e) {
            System.out.println("¡Error!" + e);
        } finally{
            System.out.println("Operación concluida");
        }

    }

}

/*
 * Clase para las funciones lógicas de una lavadora
 *
 */

package Librerias;

public class LLFunciones {
    
    private int kilos = 0, llenadoCompleto = 0, TipoDeRopa = 0, LavadoCompleto = 0, SecadoCompleto = 0; 
    
    
    
    public LLFunciones(int kilos, int TipoDeRopa){
        this.kilos = kilos;
        this.TipoDeRopa = TipoDeRopa;      
    }
    private void Llenado(){
        if(getKilos() <= 12){
            setLlenadoCompleto(1);
            System.out.println("Llenando");
            System.out.println("Llenado completo");            
        } else {
            System.out.println("La carga de ropa es muy pesada, reduce la carga");
        }
    }
    private void Lavado(){
       Llenado();
       if(getLlenadoCompleto() == 1){
           if(TipoDeRopa == 1){
               System.out.println("Ropa Blanca / Lavado suave");
               System.out.println("Lavando...");
               LavadoCompleto = 1;
                            
           } else if (TipoDeRopa == 2){
               System.out.println("Ropa de color / lavado intenso");
               System.out.println("Lavando...");
               LavadoCompleto = 1;
               
           } else {
               System.out.println("El tipo de ropa no está disponible");
               System.out.println("Se lavará como ropa de color / lavado intenso");
               LavadoCompleto = 1;
               
           }
           
       }
    }
    private void Secado(){
    Lavado();
    if(LavadoCompleto == 1){
        System.out.println("Secando...");
        SecadoCompleto = 1;
                
    }
    }
    public void CicloFinalizado(){
        Secado();
        if(SecadoCompleto == 1){
            System.out.println("Tu Ropa está limpia");
            
        }
    }
    
    /*Setter & Getter*/
    
    public int getTipoDeRopa(){
        return TipoDeRopa;
    }
    public void setTipoDeRopa(int TipoDeRopa){
        this.TipoDeRopa = TipoDeRopa;
        
    }

    /**
     * @return the kilos
     */
    public int getKilos() {
        return kilos;
    }

    /**
     * @param kilos the kilos to set
     */
    public void setKilos(int kilos) {
        this.kilos = kilos;
    }

    /**
     * @return the llenadoCompleto
     */
    public int getLlenadoCompleto() {
        return llenadoCompleto;
    }

    /**
     * @param llenadoCompleto the llenadoCompleto to set
     */
    public void setLlenadoCompleto(int llenadoCompleto) {
        this.llenadoCompleto = llenadoCompleto;
    }
    
    
}

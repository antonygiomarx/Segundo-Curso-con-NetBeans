package clases;

public class ClasePrincipal {

    public static void main(String[] args) {

       ProcesoUno hilo1 = new ProcesoUno();/*Solamente al heredar desde Thread*/
       ProcesoUno hilo3 = new ProcesoUno();
       Thread hilo2 = new Thread(new ProcesoDos());/*Solamente aplica cuando se utiliza implements*/
       
       hilo1.start();
       hilo2.start();
       hilo3.start();
       

    }
}

package com.giomar.convarabicoaromano;

import java.util.Scanner;

public class Conversor {

    public void conversor(){

        Scanner sc = new Scanner(System.in);

        String Unidad[] = {"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"};
        String Decena[] = {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"};
        String Centena[] = {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"};
        /*int UnidadA[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        int DecenaA[] = {10, 20, 30, 40, 50, 60, 70, 80, 90};
        int CentenaA[] = {100, 200, 300, 400, 500, 600, 700, 800, 900};
        */
        
        int bandera = 0, seleccion = 0;
        

        do {
            
                System.out.println("----------------------------------------");
                System.out.println("Escoge la opción de tu preferencia: ");
                System.out.println("1. Convertir de número arábico a romano");
                System.out.println("2. Convertir de número romano a arábico(pendiente)");
                System.out.println("3. Salir");
                System.out.println("-----------------------------------------");
                seleccion = sc.nextInt();

                if (seleccion == 1) {
                    System.out.println("-------------------------------");
                    System.out.println("¿Cual es el número a convertir?");
                    System.out.println("Ingresa numero entre 1 y 999");
                    System.out.println("-------------------------------");
                    int N = sc.nextInt();
                    int u = N % 10;
                    int d = (N / 10) % 10;
                    int c = N / 100;
                    if (N >= 100) {
                        System.out.println(Centena[c] + Decena[d] + Unidad[u]);
                    } else {
                        if (N >= 10) {
                            System.out.println(Decena[d] + Unidad[u]);
                        } else {
                            System.out.println(Unidad[N]);
                        }
                    }
                }
                /*if (seleccion == 2) {
                    System.out.println("-------------------------------");
                    System.out.println("¿Cual es el número a convertir?");
                    System.out.println("Ingresa numero entre 1 y 999");
                    System.out.println("-------------------------------");
                    
                    }
                */
                if (seleccion == 3) {
                    System.out.println("------------------------------------");
                    System.out.println("¡Gracias por utilizar este servicio!");
                    System.out.println("-------------------------------------");
                    bandera = 2;
                }
        } while (bandera != 2);

    }

}
